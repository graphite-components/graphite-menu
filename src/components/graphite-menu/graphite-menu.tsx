import { Component, Prop, Element, Watch, Listen } from '@stencil/core';
const isParent = (parent, child) => {
  let node = child.parentNode;
  while (node !== null) {
      if (node === parent) {
          return true;
      }
      node = node.parentNode;
  }
  return false;
}

@Component({
  tag: 'graphite-menu',
  shadow: false
})
export class GraphiteMenu {
  @Element() dropdown : HTMLElement
  button! : HTMLButtonElement
  controls! : HTMLElement
  
  // set open to true to have it open, set it to false to have it closed
  @Prop({
    mutable: true,
    reflectToAttr: true
  }) open : boolean
  @Watch('open')
  handleOpen() {
    this.button.setAttribute('aria-expanded', `${this.open}` )
    this.controls.hidden = !this.open
  }

  // set open-while-focused to false 
  // to prevent it from closing when focusing a different element
  @Prop() openWhileFocused? : boolean
  @Listen('click', {
    passive: true,
    target: 'document',
  })
  handleClick(event){
    // closes graphite-menu if click is outside graphite menu
    if(
      this.open 
      && this.openWhileFocused !== false 
      && isParent(this.dropdown, event.target) === false
    ){
      this.open = false;
    }
  }

  componentWillLoad(){
    // graphite dropdown assumes that the first button is the control
    this.button = this.dropdown.querySelector('button[aria-controls]')
    // graphite dropdown assumes that 
    this.controls = document.getElementById(this.button.getAttribute('aria-controls'))
  }
  componentDidLoad(){
    this.handleOpen()
    this.button.addEventListener('click', () => {
      this.open = !this.open
    })
  }
}
