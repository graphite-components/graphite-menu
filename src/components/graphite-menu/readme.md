# graphite-menu

Graphite menu follows most of the best practices laid out here: https://inclusive-components.design/menus-menu-buttons/

## Getting started
To get started, just include a `<button>` element as the first element with the `aria-controls` attribute assigned to the very next element. If you'd like it closed when you start, set open to false like so: 

```
<graphite-menu open="false">
    <button aria-controls="menu">Menu</button>
    <ul id="menu">
        <li>etc</li>
    </ul>
</graphite-menu>
```
## What Happens

After hydration, if the menu is open, it'll add `aria-expanded="true"` to the button, otherwise if it's closed, it'll add `aria-expanded="false` to the button and `hidden` to the element you pointed to with aria-controls

## Open Attribute

The `open` attribute controls whether or not the menu is open. Much like the open attribute on the details/summary element.

## Open While Focused

The `open-while-focused` attribute controls whether the menu closes if you it no longer has focus within it or if it stays open until a user clicks it again. This is useful, if you're using multiple graphite-menus. 

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description | Type      | Default     |
| ------------------ | -------------------- | ----------- | --------- | ----------- |
| `open`             | `open`               |             | `boolean` | `undefined` |
| `openWhileFocused` | `open-while-focused` |             | `boolean` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
